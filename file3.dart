void main() {
  //storing data of MTN Business App of the Year
  var app1 = new Developers();

  app1.name = 'Ambani Africa';
  app1.sector = 'Education & Gaming';
  app1.developer = 'Mukundi Lambani';
  app1.year = 2021;

  var app2 = new Developers();

  app2.name = 'EasyEquities';
  app2.sector = 'Consumer Solution';
  app2.developer = 'Charles Savage';
  app2.year = 2020;

  var app3 = new Developers();

  app3.name = 'Naked Insurance';
  app3.sector = 'Financial Solution';
  app3.developer = 'Sumarie Greybe, Ernest North & Alex Thomson';
  app3.year = 2019;

  var app4 = new Developers();

  app4.name = 'Khula';
  app4.sector = 'Agricutural Solution';
  app4.developer = 'Matthew Piper';
  app4.year = 2018;

  var app5 = new Developers();

  app5.name = 'Wumdrop';
  app5.sector = 'Enterprise App Solution';
  app5.developer = 'Benjamin Claassen & Muneeb Samuels';
  app5.year = 2015;

  app1.printDeveloperInformation();
  app2.printDeveloperInformation();
  app3.printDeveloperInformation();
  app4.printDeveloperInformation();
  app5.printDeveloperInformation();
}

//class
class Developers {
  String? name;
  String? sector;
  String? developer;
  int? year;

  //function
  void printDeveloperInformation() {
    print("MTN Business App of the Year Awards:" '\n');

    print("Name of the app: $name.".toUpperCase());
    print("Sector/Category: $sector");
    print("Developer's name: $developer");
    print("Year it won MTN Business App Award: $year" '\n');
  }
}
