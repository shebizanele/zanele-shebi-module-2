void main() {
  //an array to store all the winning apps of MTN Business App of the Year since 2012
  var pastwinners = [
    'FNB Banking APP',
    'SnapScan',
    'LIVE Inspect',
    'Wumdrop',
    'Domestly',
    'Shyft',
    'Khula',
    'Naked Insurance',
    'EasyEquities',
    'Ambani Africa',
  ];

//sort & print the apps by name
  pastwinners.sort();
  print('MTN Business App of the Year Winners from 2012-2021:' '\n');

  print('$pastwinners' '\n');

  //2017 & 2018 winning apps
  print('The 2017 MTN Business App Winner is:');
  print(pastwinners[7]);
  print('The 2018 MTN Business App Winner is:');
  print(pastwinners[4]);

  //total no. of apps
  print('The total number of Apps from Array is:');
  print(pastwinners.length);
}
